//
//  DacodesResponse.swift
//  DCAPIManager
//
//  Created by Pedro Romero on 9/27/18.
//

import Foundation

public struct DCResponseError: Codable {
    public var code: Int
    public var message: String
    public var statusCode: Int?
    
    public init(code: Int, message: String, statusCode: Int?) {
        self.code = code
        self.message = message
        self.statusCode = statusCode
    }
}
