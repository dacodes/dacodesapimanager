//
//  DCMultipartModel.swift
//  Pods
//
//  Created by Abraham Escamilla Pinelo on 17/10/18.
//

import Foundation

public struct DCMultipartModel {
    public var fileExtension: String
    public var key: String
    public var fileName: String
    public var mimeType: String
    public var data: Data
    
    public init(key: String, fileName: String, mimeType: String, data: Data, fileExtension: String) {
        self.key = key
        self.fileName = fileName
        self.mimeType = mimeType
        self.data = data
        self.fileExtension = fileExtension
    }
}
