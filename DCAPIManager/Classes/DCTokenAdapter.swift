//
//  AccessTokenAdapter.swift
//  DCAPIManager
//
//  Created by Pedro Romero on 9/27/18.
//

import Foundation
import Alamofire

public class DCTokenAdapter: RequestAdapter {
    private let accessToken: String
    
    public init(accessToken: String) {
        self.accessToken = accessToken
    }
    
    public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue(self.accessToken, forHTTPHeaderField: "Authorization")
        
        return urlRequest
    }
}
