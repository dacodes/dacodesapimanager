//
//  PlaceAPI.swift
//  DCAPIManager_Example
//
//  Created by Pedro Romero on 10/2/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import Alamofire
import DCAPIManager

enum PlaceAPI: APIConfiguration {
    
    case places()
    
    var method: HTTPMethod {
        switch self {
        case .places:
            return .get
        }
    }
    
    var path: String {
        var id = 0
        if let user = User.get() {
            id = user.id
        }
        switch self {
        case .places:
            return "places/\(id)"
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .places:
            return URLEncoding.default
        
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .places:
            return nil
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        var url = try DCAPIManager.shared.host.asURL()
        url.appendPathComponent(DCAPIManager.shared.apiVersion)
        url.appendPathComponent(path)
        
        var urlRequest = try URLRequest(url: url, method: method)
        urlRequest = try encoding.encode(urlRequest, with: parameters)
        
        return urlRequest
    }
}

// MARK: - API Calls
extension Place {
    static func getPlaces(completion: @escaping (DCResponseError?, [Place]?)->Void) {
        DCAPIManager.shared.request(urlRequest: PlaceAPI.places()) { (error, placesData: PlaceData?) in
            if let error = error {
                completion(error, nil)
            } else {
                if let placesData = placesData {
                    let places = placesData.data
                    completion(nil, places)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
}
