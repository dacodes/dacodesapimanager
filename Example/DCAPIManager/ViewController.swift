//
//  ViewController.swift
//  DCAPIManager
//
//  Created by pedro-romero on 09/26/2018.
//  Copyright (c) 2018 pedro-romero. All rights reserved.
//

import UIKit
import DCAPIManager

class ViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        User.login(email: "pedro.romero@dacodes.com.mx", password: "123456") { (error, user) in
//            if let error = error {
//                print("error con el login \(error.message)")
//            } else {
//                print("login success \(String(describing: user))")
//            }
//        }
        
//        User.register(name: "Pedro Romero", email: "pedro_128@gmail.com", password: "123456") { (error, user) in
//            if let error = error {
//                print("error register \(error.message)")
//            } else {
//                print("register success \(String(describing: user))")
//            }
//        }
        
//        if var user = User.get() {
//            user.name = "Pedro Romero Ojeda"
//            user.phone = "9991515334"
//            let photoImage = UIImage(named: "fotoMaestria")
//            let photoData = UIImageJPEGRepresentation(photoImage!, 0.7)
//            user.update(photoData: photoData) { (error, user) in
//                if let error = error {
//                    print("error get profile \(error.message)")
//                } else {
//                    print("get profile success \(String(describing: user))")
//                }
//            }
//        }
        
//        if let user = User.get() {
//            user.get { (error, user) in
//                if let error = error {
//                    print("error get profile \(error.message)")
//                } else {
//                    print("get profile success \(String(describing: user))")
//                }
//            }
//        }
        
//        Place.getPlaces { (error, places) in
//            if let error = error {
//                print("error getPlaces \(error.message)")
//            } else if let places = places {
//                for place in places {
//                    print("place: \(String(describing: place.title))")
//                }
//            }
//        }
//        guard let image = UIImage(named: "fotoMaestria"), let data = UIImageJPEGRepresentation(image, 0.5) else {return}
//            User.testUpload(imageData: data, completion: { (error) in
//
//                print(error)
//
//            })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

