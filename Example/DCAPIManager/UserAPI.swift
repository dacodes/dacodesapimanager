//
//  UserAPI.swift
//  DCAPIManager_Example
//
//  Created by Pedro Romero on 9/27/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import Alamofire
import DCAPIManager

enum UserAPI: APIConfiguration {
    
    case login(parameters: Parameters)
    case register(parameters: Parameters)
    case get()
    case update(parameters: Parameters)
    case testUpload
    
    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .register:
            return .post
        case .get:
            return .get
        case .update:
            return .put
        case .testUpload:
            return .post
        }
    }
    
    var path: String {
        var id = 0
        if let user = User.get() {
            id = user.id
        }
        switch self {
        case .login:
            return "login/"
        case .register:
            return "register/"
        case .get:
            return "appusers/\(id)"
        case .update:
            return "appusers/\(id)"
            
        case .testUpload:
            return "dishes/"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .login(let params):
            return params
        case .register(let params):
            return params
        case .update(let params):
            return params
        
        default:
            return nil
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .login, .register, .get, .update, .testUpload:
            return URLEncoding.default
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        var url = try DCAPIManager.shared.host.asURL()
        url.appendPathComponent(DCAPIManager.shared.apiVersion)
        url.appendPathComponent(path)
        
        var urlRequest = try URLRequest(url: url, method: method)
        urlRequest = try encoding.encode(urlRequest, with: parameters)
        
        return urlRequest
    }
}

// MARK: - API Calls
extension User {
    
    static func testUpload(imageData: Data,completion: @escaping (_ error: DCResponseError?) -> Void) {
        
        let parametros: Parameters = [
            "sort": 1,
            "name": "Nuevo platillo Prueba",
            "description": "Prueba",
            "price": 49,
            "is_active": false,
            "restaurant": 29
        ]
        
        let model = DCMultipartModel(key: "image", fileName: "asdfas", mimeType: "image/jpeg", data: imageData, fileExtension: ".jpg")
        let multiparts: [DCMultipartModel] = [model]
        
        DCAPIManager.shared.upload(urlRequest: UserAPI.testUpload, parameters: parametros, multipartForms: multiparts) { (error, user: User?) in
            
            DispatchQueue.main.async {
                completion(error)
            }
            
        }
        
    }
    
    func get(completion: @escaping (DCResponseError?, User?)->Void) {
        DCAPIManager.shared.request(urlRequest: UserAPI.get()) { (error, userData: UserData?) in
            if let error = error {
                completion(error, nil)
            } else {
                if let userData = userData {
                    let user = userData.data
                    user.save()
                    completion(nil, user)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    static func login(email: String, password: String, completion: @escaping (DCResponseError?, User?)->Void) {
        let parameters: Parameters = [
            "email": email,
            "password": password
        ]
        DCAPIManager.shared.request(urlRequest: UserAPI.login(parameters: parameters)) { (error, userData: UserData?) in
            if let error = error {
                completion(error, nil)
            } else {
                if let userData = userData {
                    let user = userData.data
                    user.save()
                    completion(nil, user)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    static func register(name: String, email: String, password: String, completion: @escaping (DCResponseError?, User?)->Void) {
        let parameters: Parameters = [
            "name": name,
            "email": email,
            "password": password
        ]
        DCAPIManager.shared.request(urlRequest: UserAPI.register(parameters: parameters)) { (error, userData: UserData?) in
            if let error = error {
                completion(error, nil)
            } else {
                if let userData = userData {
                    let user = userData.data
                    user.save()
                    completion(nil, user)
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
    
    func update(photoData: Data?, completion: @escaping (DCResponseError?, User?) -> Void) {
        var filename: String?
        var fileKey: String?
        var mimeType: String?
        if photoData != nil {
            let userId = self.id
            let timestamp = Date().timeIntervalSinceNow
            filename = "\(userId)_\(timestamp).jpeg"
            fileKey = "photo"
            mimeType = "image/jpeg"
        }
        let parameters: Parameters = [
            "name": self.name,
            "email": self.email,
            "phone": self.phone ?? ""
        ]

        DCAPIManager.shared.upload(urlRequest: UserAPI.update(parameters: parameters), parameters: parameters, fileData: photoData, fileName: filename, fileKey: fileKey, mimeType: mimeType) { error, data in
            if let error = error {
                completion(error, nil)
            } else {
                if let data = data {
                    let decoder = JSONDecoder()
                    let userData = try? decoder.decode(UserData.self, from: data)
                    if let userData = userData {
                        let user = userData.data
                        user.save()
                        completion(nil, user)
                    } else {
                        completion(nil, nil)
                    }
                } else {
                    completion(nil, nil)
                }
            }
        }
    }
}
