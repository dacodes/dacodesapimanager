//
//  Place.swift
//  DCAPIManager_Example
//
//  Created by Pedro Romero on 10/2/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation

struct Place: Codable {
    var id: Int
    var lat: Double
    var lng: Double
    var title: String?
    var description: String?
    var createdAt: String
    var updatedAt: String
    var appUser: Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case lat
        case lng
        case title
        case description
        case createdAt = "createdat"
        case updatedAt = "updatedat"
        case appUser = "appuser"
    }
}

struct PlaceData: Codable {
    var data: [Place]
}
