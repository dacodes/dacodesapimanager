//
//  User.swift
//  DCAPIManager_Example
//
//  Created by Pedro Romero on 9/27/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation

struct User: Codable {
    var id: Int
    var name: String
    var email: String
    var token: String
    var photo: String?
    var phone: String?
    
    static func get() -> User? {
        if let userData = UserDefaults.standard.value(forKey:"user") as? Data {
            return try? PropertyListDecoder().decode(User.self, from: userData)
        }
        return nil
    }
    
    func save() {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(self), forKey:"user")
    }
    
    func delete() {
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.synchronize()
    }
}

struct UserData: Codable {
    var data: User
}
