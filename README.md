# DCAPIManager

[![CI Status](https://img.shields.io/travis/pedro-romero/DCAPIManager.svg?style=flat)](https://travis-ci.org/pedro-romero/DCAPIManager)
[![Version](https://img.shields.io/cocoapods/v/DCAPIManager.svg?style=flat)](https://cocoapods.org/pods/DCAPIManager)
[![License](https://img.shields.io/cocoapods/l/DCAPIManager.svg?style=flat)](https://cocoapods.org/pods/DCAPIManager)
[![Platform](https://img.shields.io/cocoapods/p/DCAPIManager.svg?style=flat)](https://cocoapods.org/pods/DCAPIManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DCAPIManager is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DCAPIManager'
```

## Author

Pedro Iván Romero Ojeda, pedro.romero@dacodes.com.mx

## License

DCAPIManager is available under the MIT license. See the LICENSE file for more info.
